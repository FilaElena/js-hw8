let lang = document.querySelector('html');
console.log (lang);
lang.setAttribute('lang', 'en');

let title = document.createElement('title');
title.innerHTML = 'Choose your option';

let metaUtf8 = document.createElement('meta');
metaUtf8.setAttribute('charset', 'UTF-8');

document.head.insertBefore(title, document.querySelector('script'));
document.head.insertBefore(metaUtf8, document.head.firstElementChild);

let firstLink = document.createElement('link');
firstLink.setAttribute('rel', 'preconnect');
firstLink.setAttribute('href', 'https://fonts.googleapis.com');

let secondLink = document.createElement('link');
secondLink.setAttribute('rel', 'preconnect');
secondLink.setAttribute('href', 'https://fonts.gstatic.com');
secondLink.setAttribute('crossorigin', '');

let thirdLink = document.createElement('link');
thirdLink.setAttribute('href', 'https://fonts.googleapis.com/css2?family=Arvo:wght@400;700&family=Montserrat:wght@400;700&family=Open+Sans:wght@400;700&display=swap');
thirdLink.setAttribute('rel', 'stylesheet');

document.head.appendChild(firstLink);
document.head.appendChild(secondLink);
document.head.appendChild(thirdLink);

let style = document.createElement('style');
style.innerHTML = `
* {
    padding: 0;
    margin: 0;
}

.container {
    max-width: 800px;
    width: 80%;
    margin: 0 auto; 
}

h1, h2 {
    font-family: 'Arvo', serif;
    font-size: 36px;
    font-weight: 400;
}

p {
    font-family: 'Open Sans', sans-serif;
    color: #9FA3A7;
}

.sup__tittle, button {
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    font-size: 12px;
    line-height: 15px;
    letter-spacing: 2.4px;
    text-transform: uppercase;
}

.head {
    line-height: 48px;
    color: #212121;
    text-align: center;
    margin-top: 122px;
    padding-bottom: 10px;
}

.head__text {
    font-size: 14px;
    line-height: 26px;
    text-align: center;
    margin-bottom: 55px;
}

.wrapper {
    display: flex;
    text-align: center;
    padding-bottom: 139px;
}

.item {
    padding-top: 75px;
    padding-bottom: 94px;
    width: 400px;
}

.first {
    border-radius: 10px 0 0 10px;
    border: 1px solid #E8E9ED;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
}

.second {
    border-radius: 0 10px 10px 0;
    background-color: #8F75BE;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
}

.gray {
    color: #9FA3A7;
}

.yellow {
    color: #FFC80A;
}

.item__tittle {
    line-height: 46px;
    padding: 24px 0;
}

.item__tittle, .item__text {
    width: 210px;
    margin: 0 auto;
}

.black {
    color: #212121;
}

.white {
    color: #FFFFFF;
}

.item__text {
    padding-bottom: 64px;
    font-size: 12px;
    line-height: 22px;
}

.button {
    padding: 15px 23px;
    background-color: transparent;
    border: 3px solid #FFC80A;
    border-radius: 30px;
    cursor: pointer;
}
`;

document.head.appendChild(style);


let container = document.createElement ('div');
container.classList.add ('container');
document.body.appendChild (container);

let h1 = document.createElement ('h1');
h1.classList.add ('head');
h1.innerHTML = 'Choose your option';

let p1 = document.createElement('p');
p1.classList.add ('head__text');
p1.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing';

container.appendChild (h1);
container.appendChild (p1);


let wrapper = document.createElement ('div');
wrapper.classList.add ('wrapper');
container.appendChild (wrapper);

let firstItem = document.createElement ('div');
firstItem.classList.add ('item');
firstItem.classList.add ('first');
wrapper.appendChild (firstItem);


let firstSupTittle = document.createElement ('div');
firstSupTittle.classList.add ('sup__tittle');
firstSupTittle.classList.add ('gray');
firstSupTittle.innerHTML = 'Freelancer';
firstItem.appendChild (firstSupTittle);

let firstH2 = document.createElement ('h2');
firstH2.classList.add ('item__tittle');
firstH2.classList.add ('black');
firstH2.innerHTML = 'Initially designed to';
firstItem.appendChild (firstH2);

let firstItemText = document.createElement ('p');
firstItemText.classList.add ('item__text');
firstItemText.innerHTML = 'But I must explain to you how all this mistaken idea of denouncing';
firstItem.appendChild (firstItemText);

let firstButton = document.createElement ('button');
firstButton.classList.add ('button');
firstButton.classList.add ('black');
firstButton.innerHTML = 'Start here';
firstItem.appendChild (firstButton);


let secondItem = document.createElement ('div');
secondItem.classList.add ('item');
secondItem.classList.add ('second');
wrapper.appendChild (secondItem);


let secondSupTittle = document.createElement ('div');
secondSupTittle.classList.add ('sup__tittle');
secondSupTittle.classList.add ('yellow');
secondSupTittle.innerHTML = 'Studio';
secondItem.appendChild (secondSupTittle);

let secondH2 = firstH2.cloneNode (true);
secondH2.classList.remove ('black');
secondH2.classList.add ('white');
secondItem.appendChild (secondH2);

let secondItemText = firstItemText.cloneNode (true);
secondItemText.classList.add ('white');
secondItem.appendChild (secondItemText);

let secondButton = firstButton.cloneNode (true);
secondButton.classList.remove ('black');
secondButton.classList.add ('white');
secondItem.appendChild (secondButton);





